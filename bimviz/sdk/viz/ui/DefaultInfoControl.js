
BIMVIZ.UI.DefaultInfoControl = function(name, iconClass){
	BIMVIZ.UI.DefaultControl.call(this, name, iconClass);

    this.selectedId = null;
};

BIMVIZ.UI.DefaultInfoControl.prototype = Object.create(BIMVIZ.UI.DefaultControl.prototype);
BIMVIZ.UI.DefaultInfoControl.constructor = BIMVIZ.UI.DefaultInfoControl;

BIMVIZ.UI.DefaultInfoControl.prototype.onProjectLoaded = function(project){
    
    var scope = this;
    var container = this.parentDiv;


    var tablehtml = '<div id="bv_div_property_container" style="overflow: auto;">\
                            <table class="table table-no-top">\
                                <thead>\
                                    <tr >\
                                        <td style="width:150px;">\<b>\Name</b>\</td>\
                                        <td style="width:200px;">\<b>\Value</b>\</td>\
                                    </tr>\
                                </thead>\
                                <tbody>\
                                    <tr >\
                                        <td colspan="2">\<b>\None Object Selected</b>\</td>\
                                    </tr>\
                                </tbody>\
                            </table>\
                        </div>';

    container.html(tablehtml);

    function onShowElementPropertyList(event) {
        var element = event.args;
        var key = element.GlobalId;
        if (scope.selectedId == element.GlobalId)
            return;

        scope.selectedId = element.GlobalId;

        var body = container.find("table tbody");
        body.html('');


        var buinessKey = scope.engine.globalIdToBussinessKey(element.GlobalId);

        var systemSets = [];
        var systemSet = {
            Name:'Common Property',
            PropertyList:[
                {
                    Name:'温度',
                    NominalValue:"26℃"
                },
                {
                    Name:'湿度',
                    NominalValue:"50",
                },
                {
                    Name:'运行状态',
                    NominalValue:"运行",
                },
                {
                    Name:'楼层',
                    NominalValue:"13层",
                },
                {
                    Name:'编号',
                    NominalValue:"#A-13-KT-07",
                },
                {
                    Name:'上次检查时间',
                    NominalValue:"2018-6-15",
                },
                {
                    Name:'BusinessKey',
                    NominalValue:buinessKey?buinessKey:"",
                },
            ]
        };

        systemSets.push(systemSet);

        popupList(body, systemSets);
        //popupList(body, element.PropertySets);
    };

    function popupList(body, sets) {
        sets.forEach(function (set) {
            var tr = '<tr class="table_propset"><td colspan=\"2\">' + set.Name + '</td></tr>';
            body.append(tr);
            set.PropertyList.forEach(function (prop) {
                var proptr = '<tr><td>' + prop.Name + '</td><td><span>' + prop.NominalValue + '</span></td></tr>';
                body.append(proptr);
            });
        });
    }

    this.engine.addListener(BIMVIZ.EVENT.OnSelectElementPropertyList, onShowElementPropertyList);
	
};