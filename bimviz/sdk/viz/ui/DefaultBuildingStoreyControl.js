
BIMVIZ.UI.DefaultBuildingStoreyControl = function(name, iconClass){
	BIMVIZ.UI.DefaultControl.call(this, name, iconClass);

    this.enabled = false;
};

BIMVIZ.UI.DefaultBuildingStoreyControl.prototype = Object.create(BIMVIZ.UI.DefaultControl.prototype);
BIMVIZ.UI.DefaultBuildingStoreyControl.constructor = BIMVIZ.UI.DefaultBuildingStoreyControl;
BIMVIZ.UI.DefaultBuildingStoreyControl.prototype.curFloorId = null;
BIMVIZ.UI.DefaultBuildingStoreyControl.prototype.onProjectLoaded = function(project){
	var scope = this;
    var html = '<div id="bv_storeybar" class="padding-20 border-bottom-1" style="width:100%;height:70px;background-color:rgba(0, 0, 0, 0.7);">\
                <div class="input-group pull-left">\
                    <label class="switch switch-primary switch-round" >\
                        <input type="checkbox" id="bv_storeymode">\
                        <span class="switch-label" data-on="" data-off=""></span>\
                        <span style="color:white"> 楼层模式 </span>\
                    </label>\
                </div>\
            </div>\
            <div id="bv_storeyitems">\
            </div>';

    this.parentDiv.html(html);
    this.parentDiv.addClass("nopadding-left nopadding-right").removeClass("padding-20");
    this.parentDiv.css("width", "200px");
    var template = '<div class="border-bottom-1 bv_storeyitem" rel="{0}">\
                        <a style="text-decoration: none">\
                            <div class="row padding-20">\
                                <center>\
                                    <span style="color:white;" id="name">{1}</span>\
                                </center>\
                            </div>\
                        </a>\
                    </div>';


    var storeys = [{"Id":"b04","Name":"b4F"},{"Id":"b03","Name":"b3F"},{"Id":"b02","Name":"b2F"},{"Id":"b01","Name":"b1F"},
                {"Id":"Q01","Name":"Q1F"},{"Id":"Q02","Name":"Q2F"},{"Id":"Q03","Name":"Q3F"},{"Id":"Q04","Name":"Q4F"}, {"Id":"Q05","Name":"Q5F"},{"Id":"Q06","Name":"Q6F"},
                    {"Id":"A07","Name":"A7F"},{"Id":"A8","Name":"A8F"},{"Id":"A09","Name":"A9F"},{"Id":"A10","Name":"A10F"},{"Id":"A11","Name":"A11F"},
                    {"Id":"A12","Name":"A12F"},{"Id":"A13","Name":"A13F"},{"Id":"A14","Name":"A14F"},{"Id":"A15","Name":"A15F"},{"Id":"A16","Name":"A16F"},
                    {"Id":"A17","Name":"A17F"},{"Id":"A18","Name":"A18F"},{"Id":"A19","Name":"A19F"},{"Id":"A20","Name":"A20F"},{"Id":"A21","Name":"A21F"},
                    {"Id":"A22","Name":"A22F"},{"Id":"A23","Name":"A23F"},{"Id":"A24","Name":"A24F"},{"Id":"A25","Name":"A25F"},{"Id":"A26","Name":"A26F"},
                    {"Id":"A27","Name":"A27F"},
                    {"Id":"B07","Name":"B7F"},{"Id":"B08","Name":"B8F"},{"Id":"B09","Name":"B9F"},{"Id":"B10","Name":"B10F"},{"Id":"B11","Name":"B11F"},
                    {"Id":"B12","Name":"B12F"},{"Id":"B13","Name":"B13F"},{"Id":"B14","Name":"B14F"},{"Id":"B15","Name":"B15F"},{"Id":"B16","Name":"B16F"},
                    {"Id":"B17","Name":"B17F"},{"Id":"B18","Name":"B18F"},{"Id":"B19","Name":"B19F"},{"Id":"B20","Name":"B20F"},{"Id":"A21","Name":"B21F"},
                    {"Id":"B22","Name":"B22F"},{"Id":"B23","Name":"B23F"},{"Id":"B24","Name":"B24F"},{"Id":"B25","Name":"B25F"},{"Id":"A26","Name":"B26F"},
                    {"Id":"B27","Name":"B27F"}
                    ]
    var container = $('#bv_storeyitems');

    container.html('');
    storeys.forEach(function (item) {
        var text = template.format(item.Id, item.Name);
        container.append(text);
    });

     $("#bv_storeymode").change(function () {
            scope.enabled = $(this).prop("checked");

            if (scope.enabled == false) {
                storeys.forEach(function (other) {
                    bimEngine.showBuildingStorey(other.Id, true);     
                });

                var files = project.files;
                for (var i = 0; i < files.length; i++) {
                    var bimfile = files[i];
                    //console.log(bimfile.info);
                    var filename = bimfile.FileName;
                   // console.log(bimfile.FileId);
                    
                    
                    scope.engine.showFile(bimfile.FileId,true);
      
                    //var size = (bimfile.FileSize / 1024 / 1024).toFixed(1) + "MB";
                    //var text = template.format(bimfile.FileId, bimfile.FileName, bimfile.LastTime, size, icon);
            
                    //treecontainer.append(text);
                }
                bimEngine.resetCameraLook(true);
                BIMVIZ.UI.DefaultBuildingStoreyControl.prototype.curFloorId = null;
            }

            scope.engine.showType('IfcSpace', false);
        });

    $('.bv_storeyitem a').click(function (e) {
        var parent = $(this).parents('.bv_storeyitem');
        var id = parent.attr('rel');
        var Name =$.trim(parent.text());
        if (scope.enabled == false)
            return;

        var files = project.files;
        for (var i = 0; i < files.length; i++) {
            var bimfile = files[i];
            //console.log(bimfile);
            //console.log(bimfile.info);
            var filename = bimfile.FileName;
            
    
            if(filename.indexOf(id)!=-1)// !=-1含有 ==-1不含有
            {
                scope.engine.showFile(bimfile.FileId,true);

     
                var mgr = scope.engine.getCameraBookmarkManager();
                mgr.getList(function(success, list){
                    if(success){
        
                        list.forEach(function (item) {

                            if(item.name == Name)
                            {
                                
                                var pos = item.position;
                                var target = item.target;
                                scope.engine.flyFromTo(pos, target);
                            }
                        });
                    }
                });
     

            }
            else
            {
                scope.engine.showFile(bimfile.FileId,false);

            }
            BIMVIZ.UI.DefaultBuildingStoreyControl.prototype.curFloorId = id;
           console.log(BIMVIZ.UI.DefaultBuildingStoreyControl.prototype.curFloorId);
            /*
            
           if(filename.indexOf("13")!=-1)// !=-1含有 ==-1不含有
           {
               
               if(filename.indexOf("B")!=-1)
               {
                   scope.engine.showFile(bimfile.FileId,false);
               }
               else
               {
                   scope.engine.showFile(bimfile.FileId,true);
               }

           }
           else
           {

               scope.engine.showFile(bimfile.FileId,false);

           }
           BIMVIZ.UI.DefaultBuildingStoreyControl.prototype.curFloor = 13;
           console.log( scope.engine);
           scope.engine.setLookFrom(BIMVIZ.BoxPosition.TopFrontRight, true);
*/
            //var size = (bimfile.FileSize / 1024 / 1024).toFixed(1) + "MB";
            //var text = template.format(bimfile.FileId, bimfile.FileName, bimfile.LastTime, size, icon);
    
            //treecontainer.append(text);
        }

    });

};